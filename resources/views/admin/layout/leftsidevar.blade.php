<div class="left-side-menu">

    <div class="slimscroll-menu">

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                <li class="menu-title">Navigation</li>

                <li>
                    <a href="javascript: void(0);">
                        <i class="fe-airplay"></i>
                        <span class="badge badge-success badge-pill float-right">4</span>
                        <span> Product </span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                           <a href="{{url('/product')}}">All product</a>
                        </li>
                        <li>
                           <a href="{{url('/create')}}">Create</a>
                        </li>
                     
                    </ul>
                </li>

                <li class="menu-title mt-2">Components</li>

            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>