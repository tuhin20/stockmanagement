<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Stock Management</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin/assets/ theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('admin/assets/images/favicon.ico')}}">

        <!-- plugin css -->
        <link href="{{asset('admin/assets/libs/jquery-vectormap/jquery-jvectormap-1.2.2.css')}}" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="{{asset('admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />

    </head>

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Topbar Start -->
          @include('admin.layout.navar')
            <!-- end Topbar -->

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">
                @include('admin.layout.leftsidevar')
            </div>
        
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->
           
            <div class="content-page">
               <div class="row">
                 <div class="col-8">
                <div class="card-box">
                         
                           
                           
                                        </div>

                                      
                                       

                                
                                </div>
                            </div>
                        </div>

                </div> <!-- content -->

                <!-- Footer Start -->
             @include('admin.layout.footer')
                <!-- end Footer -->

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

        <!-- Right Sidebar -->
        <div class="right-bar">
            <div class="rightbar-title">
                <a href="javascript:void(0);" class="right-bar-toggle float-right">
                    <i class="dripicons-cross noti-icon"></i>
                </a>
                <h5 class="m-0 text-white">Settings</h5>
            </div>
            <div class="slimscroll-menu">
                <!-- User box -->
                <div class="user-box">
                    <div class="user-img">
                        <img src="{{asset('admin/assets/images/users/user-1.jpg')}}" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                        <a href="javascript:void(0);" class="user-edit"><i class="mdi mdi-pencil"></i></a>
                    </div>
            
                    <h5><a href="javascript: void(0);">Geneva Kennedy</a> </h5>
                    <p class="text-muted mb-0"><small>admin/assets/ Head</small></p>
                </div>

                <!-- Settings -->
                <hr class="mt-0" />
                <h5 class="pl-3">Basic Settings</h5>
                <hr class="mb-0" />

                <div class="p-3">
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox1" type="checkbox" checked>
                        <label for="Rcheckbox1">
                            Notifications
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox2" type="checkbox" checked>
                        <label for="Rcheckbox2">
                            API Access
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox3" type="checkbox">
                        <label for="Rcheckbox3">
                            Auto Updates
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox4" type="checkbox" checked>
                        <label for="Rcheckbox4">
                            Online Status
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-0">
                        <input id="Rcheckbox5" type="checkbox" checked>
                        <label for="Rcheckbox5">
                            Auto Payout
                        </label>
                    </div>
                </div>

                <!-- Timeline -->
                <hr class="mt-0" />
                <h5 class="pl-3 pr-3">Messages <span class="float-right badge badge-pill badge-danger">25</span></h5>
                <hr class="mb-0" />
               

            </div> <!-- end slimscroll-menu-->
        </div>
        <!-- /Right-bar -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="{{asset('admin/assets/js/vendor.min.js')}}"></script>

        <!-- Plugins js-->
        <script src="{{asset('admin/assets/libs/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/jquery-vectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/jquery-vectormap/jquery-jvectormap-world-mill-en.js')}}"></script>

        <!-- Dashboard 2 init -->
        <script src="{{asset('admin/assets/js/pages/dashboard-2.init.js')}}"></script>

        <!-- App js-->
        <script src="{{asset('admin/assets/js/app.min.js')}}"></script>
        
    </body>
</html><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Stock Management</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin/assets/ theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('admin/assets/images/favicon.ico')}}">

        <!-- plugin css -->
        <link href="{{asset('admin/assets/libs/jquery-vectormap/jquery-jvectormap-1.2.2.css')}}" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="{{asset('admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />

    </head>

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Topbar Start -->
          @include('admin.layout.navar')
            <!-- end Topbar -->

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">
                @include('admin.layout.leftsidevar')
            </div>
        
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->
           
            <div class="content-page">
               <div class="row">
                    <div class="col-8">
                        <div class="card-box">
                        <form id="add_form" autocomplete="off">
                            <table id="stock_table" class="table table-sm">
                                <thead>
                                    <tr>
                                    <th width="10%">Sl</th>
                                    <th width="30%">Item</th>
                                    <th width="30%">Soctk In</th>
                                    <th width="30%">Present Stock</th>
                                    <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <select data-id="1"  id="porduct1" class="form-control productId" name="productId[]">
                                                <option value="0">Select Product</option>
                                                @foreach($allProduct as $product)
                                                <option value="{{$product->id}}">{{$product->name}}</option>

                                                @endforeach()
                                            </select>
                                        </td>
                                        
                                        <td><input type="text" value="" id="quantity" name="quantity[]" class="form-control" style="height: 35px !important;"></td>

                                        <td><input readonly="" type="text" id="inStock1" class="form-control inStock" style="height: 35px !important;"></td>
                                        
                                        
                                        <td><span id="add_stock" style="font-size: 25px;color: #82ae46" class="nav-icon far fa-plus-square"></span></td>
                                    </tr>
                                </tbody>
                            </table>  
                            
                            <button type="button" id="add_btn" class="btn btn-primary">Submit</button>          
                             </form>      
                        </div>                  
                    </div>
                </div>
            </div>

            </div> <!-- content -->

            <script>
 var count = 1;
 $('#add_stock').click(function () {
    count ++;
 $('#stock_table > tbody:last').append('<tr><td>'+count+'</td><td><select id="product'+count+'" data-id="'+count+'" class="form-control productId" name="productId[]"><option value="0">Select Product</option>@foreach($allProduct as $product)<option value="{{$product->id}}">{{$product->name}}</option> @endforeach()</select></td><td><input type="text" value="" id="quantity" name="quantity[]" class="form-control" style="height: 35px !important;"></td><td><input readonly id="inStock'+count+'" class="form-control inStock" style="height: 35px !important;" type="text" /></td><td><span style="font-size: 25px;color:red" class="nav-icon far fa-minus-square delete_row"></span></td></tr>'
 );
 $(".delete_row").click(function(event) {
 $(this).parent().parent().remove();
 });

   $('.productId').change(function(){
        var productId = $(this).val();
         rowid = $(this).attr('data-id');

         /* $('.productId').each(function(i, obj) {
            if(rowid > 1){
                if(productId == $(this).val()){
                $('#product'+rowid).val(0);
                $('#inStock'+rowid).val(0);
                // alert('Product Already Selected');
                return;
             } 
            }
              
        });*/

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ url('instock') }}",
            type: 'get',
            dataType: "json",
            data: {
                productId: productId
            },
            success: function (data) {
                $('#inStock'+rowid).val(data);
            }
        });
   });
 });


 $("#add_btn").click(function () {
        $(".error_msg").html('');
        var data = new FormData($('#add_form')[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: "POST",
            url: "{{url("storestock")}}",
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data, textStatus, jqXHR) {

            }
        }).done(function () {
            $("#success_msg").html("Data Save Successfully");
            // window.location.href = "{{ url('stocklist')}}";
            location.reload();
        }).fail(function (data, textStatus, jqXHR) {
            var json_data = JSON.parse(data.responseText);
            $.each(json_data.errors, function (key, value) {
                $("#" + key).after("<span class='error_msg' style='color: red;font-weigh: 600'>" + value + "</span>");
            });
        });
    });

   $('.productId').change(function(){
        var productId = $(this).val();
         rowid = $(this).attr('data-id');

        /* $('.productId').each(function(i, obj) {
            if(rowid > 1){
                if(productId == $(this).val()){
                $('#product'+rowid).val(0);
                $('#inStock'+rowid).val(0);
                // alert('Product Already Selected');
                return;
             } 
            }
              
        });*/

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ url('instock') }}",
            type: 'get',
            dataType: "json",
            data: {
                productId: productId
            },
            success: function (data) {
                $('#inStock'+rowid).val(data);
            }
        });
   });


</script>

                <!-- Footer Start -->
             @include('admin.layout.footer')
                <!-- end Footer -->

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

        <!-- Right Sidebar -->
        <div class="right-bar">
            <div class="rightbar-title">
                <a href="javascript:void(0);" class="right-bar-toggle float-right">
                    <i class="dripicons-cross noti-icon"></i>
                </a>
                <h5 class="m-0 text-white">Settings</h5>
            </div>
            <div class="slimscroll-menu">
                <!-- User box -->
                <div class="user-box">
                    <div class="user-img">
                        <img src="{{asset('admin/assets/images/users/user-1.jpg')}}" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                        <a href="javascript:void(0);" class="user-edit"><i class="mdi mdi-pencil"></i></a>
                    </div>
            
                    <h5><a href="javascript: void(0);">Geneva Kennedy</a> </h5>
                    <p class="text-muted mb-0"><small>admin/assets/ Head</small></p>
                </div>

                <!-- Settings -->
                <hr class="mt-0" />
                <h5 class="pl-3">Basic Settings</h5>
                <hr class="mb-0" />

                <div class="p-3">
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox1" type="checkbox" checked>
                        <label for="Rcheckbox1">
                            Notifications
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox2" type="checkbox" checked>
                        <label for="Rcheckbox2">
                            API Access
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox3" type="checkbox">
                        <label for="Rcheckbox3">
                            Auto Updates
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox4" type="checkbox" checked>
                        <label for="Rcheckbox4">
                            Online Status
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-0">
                        <input id="Rcheckbox5" type="checkbox" checked>
                        <label for="Rcheckbox5">
                            Auto Payout
                        </label>
                    </div>
                </div>

                <!-- Timeline -->
                <hr class="mt-0" />
                <h5 class="pl-3 pr-3">Messages <span class="float-right badge badge-pill badge-danger">25</span></h5>
                <hr class="mb-0" />
               

            </div> <!-- end slimscroll-menu-->
        </div>
        <!-- /Right-bar -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="{{asset('admin/assets/js/vendor.min.js')}}"></script>

        <!-- Plugins js-->
        <script src="{{asset('admin/assets/libs/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/jquery-vectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/jquery-vectormap/jquery-jvectormap-world-mill-en.js')}}"></script>

        <!-- Dashboard 2 init -->
        <script src="{{asset('admin/assets/js/pages/dashboard-2.init.js')}}"></script>

        <!-- App js-->
        <script src="{{asset('admin/assets/js/app.min.js')}}"></script>
        
    </body>
</html>